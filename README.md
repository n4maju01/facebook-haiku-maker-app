# Facebook Haiku Maker App #

http://pitkamatkaeipelota.fi/haikumaker/public_html/ (requires FB login elsewhere to work correctly)

### FI ###

Facebook-sovellus, joka generoi satunnaisia haikuja käyttäjän Facebook-päivityksistä. Voit jakaa luomasi haikut kavereidesi kanssa. Vielä osittain työn alla.

### EN ###

A Facebook app that generates randomized haiku out of your Facebook posts. You can share your haikus with your friends. Still a work in progress.