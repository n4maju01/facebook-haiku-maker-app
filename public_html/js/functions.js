var data;
var vocabulary = [];

var c = document.getElementById("haikuCanvas");
var ctx = c.getContext("2d");
ctx.font = "40px Kaushan Script";

$(".makeHaiku").click(function() {
    $("#haikuArea").css("display", "block");
    $("#shareHaikuBtn").css("display", "inline-block");
    $(".changeText").html("Make another");
    $("#haikuArea").html("");
    var line1 = generateHaikuLine(5);
    var line2 = generateHaikuLine(7);
    var line3 = generateHaikuLine(5);
    updateHaikuCanvas(line1, line2, line3);
});

function updateHaikuCanvas(line1, line2, line3) {
    var bgImg = new Image();
    bgImg.onload = function() {
        ctx.drawImage(bgImg, 0, 0);
        ctx.textAlign="center";
        ctx.fillText(line1, 235, 80);
        ctx.fillText(line2, 235, 130);
        ctx.fillText(line3, 235, 180);
    };
    bgImg.src = 'img/share_haiku_bg.png';
}

function generateHaikuLine(syllableCount) {
    var haikuLine = "";

    var word = "";

    while (countSyllables(haikuLine) != syllableCount) {
        word = vocabulary[Math.floor(Math.random()*(vocabulary.length - 1))];
        
        if (haikuLine == "") {
            haikuLine += word;
        }
        else {
            haikuLine += " " + word;
        }

        if (countSyllables(haikuLine) > syllableCount) {
            haikuLine = "";
        }
    }
    $("#haikuArea").append(haikuLine + "<br>");
    return haikuLine;
}   

function countSyllables(word) {
    //console.log(word);
    //console.log(word.length);
    if (word.length == 0) {
        return 0;
    }
    
    if (word.length == 1 || word.length == 2) {
        return 1;
    }
    
    word = word.toLowerCase();
    //if(word.length <= 3) { return 1; }
    word = word.replace(/(?:[^laeiouyåäö]es|ed|[^laeiouyåäö]e)$/, '');
    word = word.replace(/^y/, '');
    return word.match(/[aeiouyåäö]{1,2}/g).length;
}

function setData(data) {
    this.data = data;
    
    var messages = "";
    var words = [];
    
    // Laitetaan data-taulukon viestit messages-merkkijonoon välillä eroteltuna
    for (var i = 0; i < data.length; i++) {
        if ('message' in data[i]) {
            messages += data[i].message + " ";
        }
    }
    
    // Otetaan merkkijonosta kaikki paitsi normaalit kirjaimet pois, splitataan
    // merkkijono sanoiksi ja laitetaan words-taulukkoon
    words = messages.replace(/[^a-zA-ZäÄöÖåÅ ]/g, "").split(" ");
    
    // Tarkastetaan, että taulukossa ei ole 'undefined' ja 'null' arvoja tai tyhjiä merkkijonoja
    for (var i = 0; i < words.length; i++) {
        if (words[i] !== undefined && words[i] !== null && words[i] !== "") {
            vocabulary.push(words[i]);
        }
    }
    
    console.log(vocabulary);
}