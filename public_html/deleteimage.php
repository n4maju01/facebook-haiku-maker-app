<?php
if(isset($_POST['fileName']))
{
    $fileName = filter_input(INPUT_POST, 'fileName', FILTER_SANITIZE_STRING);
    $delay = filter_input(INPUT_POST, 'delay', FILTER_SANITIZE_NUMBER_INT);
    $imagePath = "upload/$fileName.png";
    sleep($delay);
    unlink($imagePath);
}