<?php
if (isset($_POST['imgBase64']))
{
    // Get the data
    $imageData = filter_input(INPUT_POST, 'imgBase64', FILTER_SANITIZE_STRING);
    $fileName = str_replace(array('/', '\\'), '+', substr($imageData, 55000, 10));
    
    // Remove the headers (data:,) part.
    // A real application should use them according to needs such as to check image type
    $filteredData = substr($imageData, strpos($imageData, ",") + 1);

    // Need to decode before saving since the data we received is already base64 encoded
    $unencodedData = base64_decode($filteredData);

    //echo "unencodedData".$unencodedData;

    // Save file. This example uses a hard coded filename for testing,
    // but a real application can specify filename in POST variable
    $fp = fopen("upload/$fileName.png", 'wb');
    fwrite($fp, $unencodedData);
    fclose($fp);
}